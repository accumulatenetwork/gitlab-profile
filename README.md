# Accumulate

Accumulate is a highly unique blockchain network built to address the biggest fundamental issues with blockchain
technology:  security, validation, scaling, pruning, and integration. Accumulate is the first blockchain to be
completely organized around Decentralized Digital Identity and Identifiers (DDIIs). Accumulate Digital Identifiers (
ADIs) have powerful and novel uses within Accumulate to allow the blockchain network to be applied to a huge set of use
cases. ADIs allow smart contracts, consensus building, validator networks, and enterprise level management of digital
assets that goes beyond the simple and constrained smart contract based frameworks of other blockchains. Accumulate
anticipates a world where the blockchain is a network of blockchain services, data, and endpoints for processes
supporting payments, business, education, regulation, entertainment, social networks and more. To this end, Accumulate
will be the first indexable blockchain using URLs to index anything within Accumulate.

- Wallets & Apps
  - [Explorer](https://explorer.accumulatenetwork.io/)
  - [Mobile](https://accumulatenetwork.io/wallet/)
  - [CLI](https://gitlab.com/accumulatenetwork/core/wallet)
- Documentation
  - [User documentation](https://docs.accumulatenetwork.io/accumulate/)
  - [Architecture](https://docs.accumulatenetwork.io/core/)